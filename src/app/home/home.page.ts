import { Component, OnInit  } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  testData = [{
    description: 'Bedroom Designs',
    image: '/assets/imgs/icon-image-128.png'
  }];
  constructor(
    public router: Router
  ) {}
  ngOnInit() {
  }
  bhagavan() {
    this.router.navigate(['/design']);
  }

}
