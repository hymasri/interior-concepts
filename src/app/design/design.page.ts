import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-design',
  templateUrl: './design.page.html',
  styleUrls: ['./design.page.scss'],
})
export class DesignPage implements OnInit {
  testData = [{
    description: 'Monthly Health Checkups',
    image: '/assets/imgs/icon-image-128.png'
  },
  {
    description: 'Yoga Work Shop',
    image: '/assets/imgs/icon-image-128.png'
  }];

  constructor(public router: Router) { }

  ngOnInit() {
  }
  nxtpage() {
    this.router.navigate(['/gallery']);
  }

}
