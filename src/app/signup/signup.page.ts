import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators, AbstractControl } from '@angular/forms';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { DataService } from '../shared/services/rest-services/data.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage implements OnInit {
  formgroup: FormGroup;
  username: AbstractControl;
  email: AbstractControl;
  password: AbstractControl;
  tel: AbstractControl;
  regData: any = {};
  constructor(
    public formBuilder: FormBuilder,
    public router: Router,
    public location: Location,
    private dataService: DataService,
  ) {
    this.formgroup = this.formBuilder.group({
      username: new FormControl('', Validators.compose([
        Validators.maxLength(25),
        Validators.minLength(4),
        Validators.pattern('^(?=.*[a-zA-Z])(?=.*[0-9])[a-zA-Z0-9]+$'),
        Validators.required
      ])),

      password: new FormControl('', Validators.compose([
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(30),
      ])),
      email: new FormControl('', Validators.compose([
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(50),
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[A-Za-z0-9-.]+$')
      ])),
      tel: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('[7-9]{1}[0-9]{9}')
      ]))
    });
    this.email = this.formgroup.controls['email'];
    this.password = this.formgroup.controls['password'];
    this.tel = this.formgroup.controls['tel'];
    this.username = this.formgroup.controls['username'];
  }
  back() {
    this.location.back();
  }
  ngOnInit() {
  }
  reg() {
    console.log('Regdata:', this.regData);
    this.dataService.post('api/userReg', this.regData).subscribe(userResponse => {
      console.log(userResponse);
      this.router.navigate(['/signin']);
    });
  }
}
