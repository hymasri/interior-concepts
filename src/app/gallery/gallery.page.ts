import { Component, OnInit , ElementRef, ViewChild } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ImageModelPage } from '../image-model/image-model.page';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.page.html',
  styleUrls: ['./gallery.page.scss'],
})
export class GalleryPage implements OnInit {
  galleryType = 'regular';
  @ViewChild('slider', { read: ElementRef }) slider: ElementRef;
  images = ['1.jpg', '2.jpg', '1.jpg', '2.jpg'];
  constructor(
    private modalController: ModalController
    ) { }

  ngOnInit() {
  }
  openPreview(img) {
    this.modalController.create({
      component: ImageModelPage,
      componentProps: {
        img: img
      }
    }).then(modal => {
      modal.present();
    });
  }
}
