import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators, AbstractControl } from '@angular/forms';
import { Router } from '@angular/router';
import { DataService } from '../shared/services/rest-services/data.service';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.page.html',
  styleUrls: ['./signin.page.scss'],
})
export class SigninPage implements OnInit {
  formgroup: FormGroup;
  email: AbstractControl;
  password: AbstractControl;
  loginData: any = {};

  constructor(
    public formBuilder: FormBuilder,
    public router: Router,
    private dataService: DataService,
  ) {
    this.formgroup = formBuilder.group({
      password: new FormControl('', Validators.compose([
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(30),
      ])),
      email: new FormControl('', Validators.compose([
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(50),
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[A-Za-z0-9-.]+$')
      ]))
    });
    this.email = this.formgroup.controls['email'];
    this.password = this.formgroup.controls['password'];
  }

  ngOnInit() {
  }
  login() {
    console.log('loginData:', this.loginData);
    this.dataService.post('api/userLogin', this.loginData).subscribe(districtUsersResp => {
    // console.log(districtUsersResp);
    // this.router.navigate(['/home']);
    });
  }
  // forgot() {
  //   console.log('loginData:', this.loginData);
  //   this.router.navigate(['/forgot']);
  // }
}
