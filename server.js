'use strict'
const express = require('express')
const app = express()
const mongoose = require('mongoose')
mongoose.Promise = require('bluebird')
const bodyParser = require('body-parser')
const methodOverride = require('method-override')
const config = require('config')
const favicon = require('serve-favicon')
const compression = require('compression')
const helmet = require('helmet')
const cors = require('cors')
const cookieParser = require('cookie-parser')
const timeout = require('connect-timeout')
const sanitize = require('mongo-sanitize')
// const server = require('http')(app);
console.log('Server JS Called1')

var processexit = false
const options = {
  auto_reconnect: true,
  reconnectTries: Number.MAX_VALUE, // Never stop trying to reconnect
  reconnectInterval: 500, // Reconnect every 500ms
  poolSize: 1000, // Maintain up to 1000 socket connections
  // If not connected, return errors immediately rather than waiting for reconnect
  bufferMaxEntries: 0,
  keepAlive: 120,
  useNewUrlParser: true,
  promiseLibrary: require('bluebird')
}
console.log('Server JS Called2')
const db = mongoose.connection

db.once('error', function (err) {
  console.error('mongoose connection error' + err)
  mongoose.disconnect()
})
db.on('open', function () {
  console.log('successfully connected to mongoose')
})
db.on('reconnected', function () {
  console.log('MongoDB reconnected!')
})
db.on('disconnected', function () {
  console.log('MongoDB disconnected!', processexit)
  if (!processexit) {
    mongoose.connect(config.mongodb.uri, options)
      .then(() => console.log('connection succesful'))
      .catch((err) => console.error(err))
  }
})

// mongoose.connect(config.mongodb.uri, options);

mongoose.connect(config.mongodb.uri, options)
  .then(() => console.log('connection succesful'))
  .catch((err) => console.error(err))

config.home_directory = __dirname
const cacheoptions = {
  dotfiles: 'ignore',
  etag: false,
  extensions: ['js', 'css', 'html'],
  index: false,
  // maxAge: '1d',
  redirect: false,
  setHeaders: function (res, path, stat) {
    res.set('x-timestamp', Date.now())
  }
}

if (process.env.NODE_ENV !== 'development') {
  console.log('adding compression')
  // compression
  app.use(compression())
  // require('geoip-lite/scripts/updatedb.js');
} else {
  console.log('Server JS Called 3 DEBUG')
  mongoose.set('debug', true)
}

app.use(timeout('60s'))
app.use(helmet())
app.use(haltOnTimedout)
app.use(cookieParser())
app.use(haltOnTimedout)

app.use(bodyParser.json({
  limit: '50mb'
}))
app.use(bodyParser.json({
  type: 'application/vnd.api+json'
})) 
app.use(bodyParser.urlencoded({
  extended: true,
  limit: '50mb',
  parameterLimit: 50000
}))

app.use(methodOverride('X-HTTP-Method-Override'))
if (process.env.NODE_ENV !== 'development') {
  console.log('adding chache options')
  app.use(express.static(__dirname + '/dist', cacheoptions))
} else {
  app.use(express.static(__dirname + '/dist')) 
}
if (process.env.NODE_ENV !== 'development') {
  app.use(favicon(__dirname + '/dist/favicon.ico'))
} else {
  app.use(favicon(__dirname + '/src/favicon.ico'))
}

app.use(express.static(__dirname + '/uploads'))

function logErrors(err, req, res, next) {
  console.log('in logErrors')
  console.error(err.stack)
  next(err)
}

function clientErrorHandler(err, req, res, next) {
  console.log('in clientErrorHandler')
  if (req.xhr) {
    res.status(500).send({
      error: 'Something failed!'
    })
  } else {
    next(err)
  }
}

app.use(logErrors)
app.use(clientErrorHandler)

const whitelist = config.cros
const corsOptions = {
  origin: function (origin, callback) {
    if (whitelist.indexOf(origin) !== -1) {
      callback(null, true)
    } else {
      callback(new Error('Not allowed by CORS'))
    }
  }
}

app.use(cors(), function (req, res, next) {
  next()
})

app.use(sanitizeCustom)

function sanitizeCustom(req, res, next) {
  let bodykeys = Object.keys(req.body)
  bodykeys.forEach(function (keyObj) {
    req.body.keyObj + '' == sanitize(req.body[keyObj])
  })

  bodykeys = Object.keys(req.query)
  bodykeys.forEach(function (keyObj) {
    req.query.keyObj + '' == sanitize(req.query[keyObj])
  })
  bodykeys = Object.keys(req.params)
  bodykeys.forEach(function (keyObj) {
    req.params.keyObj + '' == sanitize(req.params[keyObj])
  })
  next()
}

// routes ==================================================
require('./restapp/routes/routes')(app)

if (process.env.NODE_ENV !== 'development') {
  setInterval(() => {
    GmailCRMService.autoMAILGenerate()
  }, 1000 * 60 * 2)
}

setInterval(() => {
  DashboardCountsService.updateCounts()
}, 1000 * 60 * 10)

app.get('*', function (req, res, next) {
  if (app.get('env') != 'development') {
    res.sendFile(__dirname + '/dist/index.html')
  } else {
    // res.redirect('http://localhost:4302/');
    res.sendFile(__dirname + '/src/index.html')
  }
})

app.internalError = function (err, code, res) {
  const error = {}
  error.message = err
  error.status = 'error'
  res.statusCode = code
  return res.send(error)
}

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  const err = new Error('Not Found')
  err.status = 404
  next(err)
})

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function (err, req, res, next) {
    res.status(err.status || 500)
    res.send({
      message: err.message,
      error: err
    })
  })
} else {
  // production error handler
  // no stacktraces leaked to user
  app.use(function (err, req, res, next) {
    res.status(err.status || 500)
    res.send({
      message: err.message,
      error: {}
    })
  })
}

app.use(function (err, req, res, next) {
  res.end(err.message) // this catches the error!!
})

function haltOnTimedout(req, res, next) {
  if (!req.timedout) next()
}

const port = normalizePort(config.port || '3333')
app.set('port', port)
// start app ===============================================
const server = app.listen(port, config.host)
// server.listen(port,config.host);
// Socket functions

function normalizePort(val) {
  const port = parseInt(val, 10)

  if (isNaN(port)) {
    // named pipe
    return val
  }

  if (port >= 0) {
    // port number
    return port
  }

  return false
}

process.on('uncaughtException', function (uncaught_exception) {
  console.error((new Date()).toUTCString() + ' uncaughtException:', uncaught_exception.message)
  console.error(uncaught_exception)
})

process.on('unhandledRejection', function (unhandled_error) {
  console.error((new Date()).toUTCString() + ' unhandledRejection:', unhandled_error.message)
  console.error(unhandled_error)
})

process.on('SIGINT', function () {
  console.log(' on exit called by node')
  processexit = true
  mongoose.connection.close()
})

process.on('uncaughtException', function (e) {
  console.error('Uncaught Exception...', e)
  console.error(e.stack)
})

console.log('Magic happens on port ' + port) // shoutout to the user
exports = module.exports = app // expose app
