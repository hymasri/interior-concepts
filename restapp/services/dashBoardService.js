const DashBoardService = module.exports
const ResponseUtils = require('../utils/ResponseUtils')
const DbConnectUtils = require('../utils/DbConnectUtils')
const usersModel = require('../models/usersModel')

DashBoardService.userReg = function userReg(apiReq, apiRes) {
  let Response = new ResponseUtils.Response(apiRes);
  console.log(apiReq.body)
  let new_admin_user = {}
  new_admin_user.email = apiReq.body.email
  usersModel.find(new_admin_user, function (temps_find_error, temps_data) {
    if (temps_find_error) {
      apiRes.send({
        success: false,
        message: 'Something went wrong while adding user.'
      })
    } else {
      console.log('temps_data::', temps_data)
      if (temps_data.length > 0) {
        console.log('NOT INERTED')
        apiRes.send({
          success: false,
          message: 'This user is already inserted'
        })
      } else {
        console.log('READY TO INERT')
        let add_user = new usersModel()
        add_user.name = apiReq.body.name
        add_user.email = apiReq.body.email
        add_user.temp_password = apiReq.body.password
        add_user.password = new usersModel().generateHash(apiReq.body.temp_password)
        add_user.mobile = apiReq.body.mobile
        add_user.user_status = apiReq.body.user_status
        add_user.user_type = apiReq.body.user_type
        console.log('CAME HEAR', add_user)
        add_user.save((error, response) => {
          if (error) {
            console.log(error)
            apiRes.send({
              success: false,
              message: 'Something went wrong while adding admin_user.'
            })
          } else {
            console.log('sucessfully inserted data', response)
            apiRes.send({
              success: true,
              message: 'Sucessfully inserted data'
            })
          }
        })
      }
    }
  })
}