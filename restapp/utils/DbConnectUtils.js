const mysql = require('mysql');
let config = require('config');

// config =  require("../../config/development");



const DbConnectUtils = module.exports;

// const selectDbWay2poolSource = mysql.createPool({
//     connectionLimit: config.mySqldb.selectdb.connectionLimit,
//     waitForConnections: config.mySqldb.selectdb.waitForConnections,
//     host: config.mySqldb.selectdb.hostname,
//     user: config.mySqldb.selectdb.username,
//     password: config.mySqldb.selectdb.password,
//     database: config.mySqldb.selectdb.database,
//     port: config.mySqldb.selectdb.port
// });

// const mainDbWay2poolSource = mysql.createPool({
//     connectionLimit: config.mySqldb.maindb.connectionLimit,
//     waitForConnections: config.mySqldb.maindb.waitForConnections,
//     host: config.mySqldb.maindb.hostname,
//     user: config.mySqldb.maindb.username,
//     password: config.mySqldb.maindb.password,
//     database: config.mySqldb.maindb.database,
//     port: config.mySqldb.maindb.port
// });
// const localDbWay2poolSource = mysql.createPool({
//     connectionLimit: config.mySqldb.localdb.connectionLimit,
//     waitForConnections: config.mySqldb.localdb.waitForConnections,
//     host: config.mySqldb.localdb.hostname,
//     user: config.mySqldb.localdb.username,
//     password: config.mySqldb.localdb.password,
//     database: config.mySqldb.localdb.database,
//     port: config.mySqldb.localdb.port
// });

let connectionPools = {};

const getConnectionPool = function getConnectionPool(dbName) {
    if (connectionPools[dbName] != null) {
        return connectionPools[dbName];
    }else{

        console.log("Pool created &&&&&&&&&&&&&&*********************", dbName);
        const dbPool = mysql.createPool({
            connectionLimit: config.mySqldb[dbName].connectionLimit,
            waitForConnections: config.mySqldb[dbName].waitForConnections,
            host: config.mySqldb[dbName].hostname,
            user: config.mySqldb[dbName].username,
            password: config.mySqldb[dbName].password,
            database: config.mySqldb[dbName].database,
            port: config.mySqldb[dbName].port
    
            // connectionLimit: 1000,
            // waitForConnections: false,
            // host: 'localhost',
            // user: 'root',
            // password: 'root',
            // database: 'way2app',
            // port: 3306
        });
        connectionPools[dbName] = dbPool;
        return dbPool;
    }

}


DbConnectUtils.getMySqlConn = function (db) {
    return new Promise((resolve, reject) => {
        db.getConnection((err, connection) => {
            if (connection)
                return resolve(connection);
            reject(err);
        });
    });
};

DbConnectUtils.execSqlQry = function (qry, params, db) {
    db = getConnectionPool(db)
    // console.log(db);
    // if( db === 'localdb'){
    //     db = localDbWay2poolSource;
    // }
    // else{
    //     db = (db == 'maindb') ? mainDbWay2poolSource : selectDbWay2poolSource;
    // }
    return new Promise((resolve, reject) => {
        DbConnectUtils.getMySqlConn(db)
            .then(connection => {
                connection.query(qry, params, (qryErr, qryResp) => {
                    connection.release();
                    if (qryResp)
                        return resolve({
                            message: "SUCCESS",
                            success: true,
                            data: qryResp
                        });
                    reject({
                        message: qryErr,
                        success: false,
                        data: []
                    });
                });
            })
            .catch(err => {
                reject({
                    message: err,
                    success: false,
                    data: []
                });
            })
    });
};


// DbConnectUtils.execSqlQry = async function (qry, params) {
//     try {
//         let connection = await DbConnectUtils.getMySqlConn();
//         let qryResp = await connection.query(qry, params);
//         return {
//             message: "SUCCESS",
//             success: true,
//             data: qryResp
//         }
//     } catch (Exception) {
//         return {
//             message: Exception,
//             success: false,
//             data: []
//         }
//     }
// };

