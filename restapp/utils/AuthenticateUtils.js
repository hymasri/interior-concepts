'use strict'
//const AppConfig = require('config')
// const ResponseUtils = require('./ResponseUtils')
// const JWT = require('jsonwebtoken')

const mysqlConnection = require('../services/mysql_connection');

// var this_file_name = 'rest-app/utils/AuthenticateUtils'

let AuthenticateUtils = module.exports

// AuthenticateUtils.userAuthentication = function (api_request, api_response, next_service) {
//   const thisFunctionName = 'AuthenticateUtils.userAuthentication'
//   let Print = new ResponseUtils.Print(this_file_name, thisFunctionName)
//   let Response = new ResponseUtils.Response(api_response)
//   // check header or url parameters or post parameters for token

//   var authenticationToken = api_request.headers['x-access-token']
//   // decode token
//   // console.log('authenticationToken>>>>', authenticationToken)
//   if (authenticationToken) {
//     // verifies secret and checks exp
//     JWT.verify(authenticationToken, AppConfig.jwtsecret, function (token_verify_error, token_data) {
//       console.log('TOKEN DATA IN AUTHENTICATION UTILS', token_data)
//       if (token_verify_error) {
//         // app.internalError('Invalid Authencation', 400, res);
//         Print.error('Error while verifing user authentication token.')
//         return Response.error('User athentication failed.', 'ERR')
//       } else {
//         // if everything is good, save to request for use in other routes
//         const now = new Date().getTime()
//         // if (!token_data.a_expiry || token_data.a_expiry < now) {
//         //   console.log('---------auth -------------', 'verification authontication expired')
//         //   const param = 'Authenticaiton token expired.Please login again'
//         //   // delete api_response.headers['x-access-token'];
//         //   return Response.error(param, 'AUTHTOKENERR', 500)
//         // } else {
//         // console.log(".....authontication failed");
//         api_request.merchant = token_data
//         next_service()
//         // }
//       }
//     })
//   } else {
//     Print.error('Authentication token not found in request.')
//     return Response.error('User athentication failed.', 'ERR')
//   }
// }

// AuthenticateUtils.userAuthenticationWithQuery = function (api_request, api_response, next_service) {
//   let thisFunctionName = 'AuthenticateUtils.userAuthenticationWithQuery'
//   let Print = new ResponseUtils.print(this_file_name, thisFunctionName)
//   let Response = new ResponseUtils.response(api_response)

//   // check header or url parameters or post parameters for token
//   var authentication_token = api_request.headers['x-access-token'] || api_request.query.token
//   // decode token
//   if (authentication_token) {
//     // verifies secret and checks exp
//     JWT.verify(authentication_token, AppConfig.jwtsecret, function (token_verify_error, token_data) {
//       if (token_verify_error) {
//         // app.internalError('Invalid Authencation', 400, res);
//         Print.error('Error while verifing user authentication token.')
//         return Response.error('User athentication failed.', 'ERR')
//       } else {
//         // if everything is good, save to request for use in other routes
//         api_request.user = token_data
//         next_service()
//       }
//     })
//   } else {
//     Print.error('Authentication token not found in request.')
//     return Response.error('User athentication failed.', 'ERR')
//   }
// }

const getUserName = function getUserName(request) {

  return request.userName;
}

const getUserPassword = function getUserPassword(request) {

  return request.password;
}

const authenicateCredentials = function authCredentials(userName, password) {
  
  let connection = mysqlConnection.getConnection();
  let table = 'users';
  let query = 'select * from ' + table + ' where name = ? Limit 1'

  return new Promise((resolve, reject) => {
    connection.query(query, [userName], function (err, res) {
      if (err) {
        return reject(new Error(err));
      }
      let statusCode = (res[0] && res[0].password === password) ? 200 : 403;
      res = null;
      return resolve(statusCode);
    })
  });
}
AuthenticateUtils.authenticateUser = async function authenticateUser(request) {

  let userName = getUserName(request);
  let password = getUserPassword(request);

  let userExistance ;
  try {
    userExistance = await authenicateCredentials(userName, password);
  }
  catch( err) {
      userExistance = 504; // Internal Error
  }
  return userExistance;
}

