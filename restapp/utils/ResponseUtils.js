'use strict'
var ResponseUtils = module.exports

// const LogsModel = require('../models/LogsModel')
var this_file_name = 'rest-app-merchant/utils/ResponseUtils.js'

ResponseUtils.Print = function (file_name, function_name) {
  let error_message = 'Error occured at ' + file_name + ' in ' + function_name + ' : '
  let log_message = 'Logging for ' + file_name + ' in ' + function_name + ' : '
  let exception_message = 'Exception occured at ' + file_name + ' in ' + function_name + ' : '

  this.error = function (message) {
    console.error(error_message)
    let error_object = {}
    error_object.type = 'ERROR'
    error_object.error_message = message
    error_object.file_name = file_name
    error_object.function_name = function_name
    console.error(error_object)
  }

  this.log = function (message) {
    console.log(log_message)
    let log_object = {}
    log_object.type = 'LOGGING'
    log_object.log_message = message
    log_object.file_name = file_name
    log_object.function_name = function_name
    console.log(log_object)
  }

  this.exception = function (message) {
    console.log(exception_message)
    let exception_object = {}
    exception_object.type = 'EXCEPTION'
    exception_object.log_message = message
    exception_object.file_name = file_name
    exception_object.function_name = function_name
    console.log(exception_object)
  }
}

var saveLogsData = function (message, code, data, type, file_name, function_name) {
  let logs_data = new LogsModel()

  logs_data.message = message
  logs_data.type = type
  logs_data.code = code
  logs_data.error_data = data
  logs_data.file_name = file_name
  logs_data.function_name = function_name

  logs_data.save(function (logs_stored_error, logs_stored) {
    if (logs_stored_error) {
      console.error('error while saving in ', logs_stored_error)
    };
  })
}

ResponseUtils.Logs = function (file_name, function_name) {
  this.error = function (message, code, data) {
    saveLogsData(message, code, data, 'ERROR', file_name, function_name)
  }

  this.log = function (message, code, data) {
    saveLogsData(message, code, data, 'LOG', file_name, function_name)
  }

  this.exception = function (message, code, data) {
    saveLogsData(message, code, data, 'EXCEPTION', file_name, function_name)
  }
}

ResponseUtils.Response = function (api_response) {
  let error_object = {}
  error_object.status_code = 400
  error_object.status = 'ERR'
  error_object.type = 'ERROR'

  let success_object = {}
  success_object.status_code = 200
  success_object.status = 'SUCCESS'
  success_object.type = 'SUCCESS'

  this.error = function (message, status, status_code) {
    error_object.message = message
    error_object.status = status
    error_object.status_code = status_code || error_object.status_code
    api_response.status(error_object.status_code).send(error_object)
  }


  this.success = function (message, data, status) {
    success_object.message = message
    success_object.status = status || success_object.status
    data || data == 0 ? success_object.data = data : 'no data'

    api_response.status(200).send(success_object)
  }

  this.errorData = function (message, status, status_code) {
    error_object.message = message
    error_object.status = status
    error_object.status_code = status_code || error_object.status_code

    return error_object
  }

  this.successData = function (message, data, status) {
    success_object.message = message
    success_object.status = status || success_object.status
    data || date == 0 ? success_object.data = data : 'no data'

    return success_object
  }
}
