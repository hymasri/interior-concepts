'use strict'
const async = require('async')
const cron = require('node-cron')
const ResponseUtils = require('./ResponseUtils')
const SMSAlertsModel = require('./../models/SMSAlertsModel')
const EmailAlertsModel = require('./../models/EmailAlertsModel')
const EncDec = require('./EncDec')
// const ConsumerLeadsModel = require('./../models/ConsumerLeadsModel')
// const PushAlertModel = require('./../models/PushAlertsModel')
// const CommonService = require('./../services/CommonService')
// const mongoose = require('mongoose')
// NODE MODULES
const AppConfig = require('config')
const MYSQL = require('mysql')
const Request = require('request')
const replaceall = require('replaceall')
// INTIALIZATIONS

const NotificationUtils = module.exports
const thisFileName = 'rest-app/utils/NotificationUtils'
NotificationUtils.notification = function () {}

NotificationUtils.sms = function (templateName, configuration, callback) {
  let thisFunctionName = 'NotificationUtils.sms'
  let Print = ResponseUtils.Print(thisFileName, thisFunctionName)
  console.log('*******TEmplate Name at NotificationUtils.sms***************', templateName, configuration)
  try {
    SMSAlertsModel.findOne({
      alert_type: templateName,
      status: 'active'
    }, function (err, resp) {
      if (err) {
        console.error('error while getting sms template with name', templateName)
        callback(err)
      } else if (resp && configuration.mobile && configuration.mobile.toString().length === 10) {
        let body = resp.alert_content
        Object.keys(configuration.replace).forEach(function (obj) {
          body = body.replace('##' + obj + '##', configuration.replace[obj])
        })
        let con = MYSQL.createConnection({
          host: AppConfig.smsdb.hostname,
          user: AppConfig.smsdb.username,
          password: AppConfig.smsdb.password,
          database: AppConfig.smsdb.database
        })
        con.connect(function (err) {
          if (err) {
            console.log('__==RECHED TO FIRST ERR==___')
            console.error(err)
          }
          console.log('configuration :: ', configuration)
          let sender_id = configuration.sender_id ? configuration.sender_id : resp.sender_id

          let sql = 'insert into user_free_message_reg(custid,mobileno,message,senddate,sender,domain) values(0,?,?,CURRENT_TIMESTAMP,?,?)'
          con.query(sql, [configuration.mobile, body, sender_id, sender_id], function (err, result) {
            con.end()
            if (err) {
              console.log('__==RECHED TO SECOND ERR==___')
              console.error(err)
              return callback(err)
            }
            callback(null, 'sms sent successfully')
          })
        })
      } else {
        console.log(resp)
        console.log('invalid mobile number length')
        callback('invalid mobile number')
      }
    })
  } catch (error) {
    Print.exception(error)
    callback(error, null)
  }
}

NotificationUtils.email = function (templateName, configuration, callback) {
  let thisFunctionName = 'NotificationUtils.email'
  let Print = ResponseUtils.Print(thisFileName, thisFunctionName)
  try {
    EmailAlertsModel.findOne({
      alert_name: templateName,
      status: 'active'
    }, function (err, resp) {
      if (err) {
        console.error('error while getting email template with name', templateName)
        callback(err)
      } else {
        // console.log('resp', resp)
        if (resp && resp.alert_route === 'api') {
          let emailmsg = {}
          emailmsg.em = configuration.email
          let body = resp.alert_content
          console.log(configuration.replace, configuration.email)
          Object.keys(configuration.replace).forEach(function (obj) {
            body = body.replace('##' + obj + '##', configuration.replace[obj])
          })
          emailmsg.body = body
          emailmsg.subj = resp.alert_subject
          emailmsg.fromname = 'Way2Money'
          emailmsg.msgid = Date.now()
          emailmsg.fromdomain = 'way2moneymail.com'
          let encemail = EncDec.encrypt(JSON.stringify(emailmsg), 'truelittle@#$')
          encemail = replaceall('/', '<->', encemail)
          Request(resp.api_url + encemail, function (error, response, body) {
            console.log('error:', error) // Print the error if one occurred
            console.log('statusCode:', response && response.statusCode) // Print the response status code if a response was received
            console.log('body:', body) // Print the HTML for the Google homepage.
            callback(null, 'email sent successfully')
          })
        } else {
          console.log('invalid alert route')
          console.log(resp)
          callback(new Error('invalid alert route'), null)
        }
      }
    })
  } catch (error) {
    Print.exception(error)
    callback(error, null)
  }
}

/* send sms to lead based on lead status */
// get lead data
// let lead_id = {};
// lead_id._id = mongoose.Types.ObjectId("5b0d55e1031247711c4be80a");
NotificationUtils.get_lead_details = function (lead_id, callback) {
  let thisFunctionName = 'NotificationUtils.get_lead_details'
  let Print = ResponseUtils.Print(thisFileName, thisFunctionName)

  try {
    let templateName
    let configuration = {}
    configuration.replace = {}

    /* getting lead details */
    ConsumerLeadsModel.findOne({
      _id: lead_id
    }, function (error, response) {
      if (error) {
        console.log('error while sending sms ', error)
        return callback(error, null)
      }
      /* setting template name and configuration */
      if (response.lead_status === 'contacted_bank' || response.lead_status === 'api_inactive') {
        templateName = 'contacted_bank'

        configuration.replace.partner_name = response.partner_name
        configuration.replace.loan_amount = response.loan_amount
      } else if (response.lead_status === 'contacted_bank_approved') {
        templateName = 'contacted_bank_approved'

        configuration.replace.partner_name = response.partner_name
        configuration.replace.loan_amount = response.loan_amount
      } else if (response.lead_status === 'contacted_bank_failed') {
        templateName = 'loan_rejected_other_offers_not_available'
        configuration.replace.partner_name = response.partner_name
        // configuration.replace.loan_amount = response.loan_amount;
      } else if (response.lead_status === 'money_transferred') {
        templateName = 'money_transferred'

        // configuration.replace.partner_name = response.partner_name;
        configuration.replace.loan_amount = response.loan_amount
      }
      // else if (response.lead_status === "documents_verified") {
      //   templateName = "documents_rejected"
      // } else if (response.lead_status === "documents_verified_failed") {
      //   templateName = "documents_rejected"
      //   configuration.replace.loan_amount = response.loan_amount;
      // }

      /* setting mobile number */
      configuration.mobile = response.mobile
      configuration.email = response.email
      console.log(configuration, templateName)

      async.parallel([
        function (callback) {
          if (process.env.NODE_ENV !== 'development') {
            NotificationUtils.send_loan_status(templateName, configuration, function (error, response) {
              return callback(null, {
                'error': error,
                'data': response
              })
            })
          } else {
            return callback(null, {
              'error': true,
              'data': 'SMS CAN NOT BE SENT LOCALLY'
            })
          }
        },
        function (callback) {
          if (process.env.NODE_ENV !== 'development') {
            NotificationUtils.email(templateName, configuration, function (error, response) {
              return callback(null, {
                'error': error,
                'data': response
              })
            })
          } else {
            return callback(null, {
              'error': true,
              'data': 'EMAIL SEND APPLIED LOCALLY'
            })
            // NotificationUtils.email(templateName, configuration, function (error, response) {
            //   return callback(null, { 'error': error, 'data': response });
            // });
          }
        }
      ], function (error, result) {
        if (error) {
          console.log('NOTIFICATIONUTILS:ERROR WHILE SENDING NOTIFICATIONS', error)
          return callback(error, null)
        }
        console.log('LOANS-NOTIFY:RESPONSE IN NOTIFICATIONS,', result)
        if (result[0].error || result[1].error) {
          let err = (result[0].error) ? result[0].error : ''
          err = (result[1].error) ? (err + ' ' + result[1].error) : err
          return callback(err, result)
        }
        return callback(null, result)
      })
    })
  } catch (exception) {
    Print.exception(exception)
    return callback(exception, null)
  }
}

// send sms for loan status
// NotificationUtils.send_loan_status_copy = function (templateName, configuration, callback) {
//   let thisFunctionName = 'NotificationUtils.send_loan_status'
//   let Print = ResponseUtils.Print(thisFileName, thisFunctionName)
//   console.log('*******Template Name at NotificationUtils.send_loan_status***************', templateName, configuration)
//   try {
//     SMSAlertsModel.findOne({
//       alert_type: templateName,
//       status: 'active'
//     }, function (err, resp) {
//       if (err) {
//         console.error('error while getting sms template with name', templateName)
//         callback(err)
//       } else if (resp && configuration.mobile && configuration.mobile.toString().length === 10) {
//         let body = resp.alert_content
//         Object.keys(configuration.replace).forEach(function (obj) {
//           body = body.replace('##' + obj + '##', configuration.replace[obj])
//         })
//         let con = MYSQL.createConnection({
//           host: AppConfig.smsdb.hostname,
//           user: AppConfig.smsdb.username,
//           password: AppConfig.smsdb.password,
//           database: AppConfig.smsdb.database
//         })
//         con.connect(function (err) {
//           if (err) {
//             console.log('__==RECHED TO FIRST ERR==___')
//             console.error(err)
//           }
//           console.log('configuration :: ', configuration)
//           let sender_id = configuration.sender_id ? configuration.sender_id : resp.sender_id
//           let sql = 'insert into user_free_message_reg(custid,mobileno,message,senddate,sender,domain) values(0,?,?,CURRENT_TIMESTAMP,?,?)'
//           con.query(sql, [configuration.mobile, body, sender_id, sender_id], function (err, result) {
//             con.end()
//             if (err) {
//               console.log('__==RECHED TO SECOND ERR==___ ')
//               console.error(err)
//               return callback(err)
//             }
//             callback(null, 'sms sent successfully')
//           })
//         })
//       } else {
//         console.log(resp)
//         console.log('invalid mobile number length')
//         callback(new Error('invalid mobile number'), null)
//       }
//     })
//   } catch (error) {
//     Print.exception(error)
//     callback(error, null)
//   }
// }

/* send sms for loan status */
NotificationUtils.send_loan_status = function (templateName, configuration, callback) {
  let thisFunctionName = 'NotificationUtils.send_loan_status'
  let Print = ResponseUtils.Print(thisFileName, thisFunctionName)
  console.log('*******Template Name at NotificationUtils.send_loan_status***************', templateName, configuration)
  try {
    SMSAlertsModel.findOne({
      alert_type: templateName,
      status: 'active'
    }, function (err, resp) {
      if (err) {
        console.error('error while getting sms template with name', templateName)
        callback(err)
      } else if (resp && configuration.mobile && configuration.mobile.toString().length === 10) {
        let alertContent = resp.alert_content
        Object.keys(configuration.replace).forEach(function (obj) {
          alertContent = alertContent.replace('##' + obj + '##', configuration.replace[obj])
        })

        let body = {
          'mobile': configuration.mobile,
          'sender_id': resp.sender_id,
          'alert_content': alertContent
        }
        console.log('BODYY OF THE SMS AFTER APPLY LOAN->', body)
        Request.post({
          url: AppConfig.smsURL,
          headers: {
            'Content-Type': 'application/json'
          },
          body: body,
          json: true
        }, function (error, smsResponse) {
          console.log('sms response', smsResponse.body)

          if (error) {
            console.log('Error from sms api\n', error)
            return callback(error, null)
          } else if (smsResponse) {
            let responseBody = smsResponse.body
            if (responseBody && responseBody.status_code && responseBody.status_code === 200) {
              console.log('***** \n\nSms Send successfully \n\n *****')
              return callback(null, 'success')
            } else {
              console.log('Error in sms response')
              return callback(new Error('Error in response from sms api'), null)
            }
          } else {
            console.log('No response from sms api\n')
            return callback(new Error('No response from sms api'), null)
          }
        })
      } else {
        console.log(resp)
        console.log('invalid mobile number length')
        return callback(new Error('invalid mobile number'), null)
      }
    })
  } catch (error) {
    Print.exception(error)
    return callback(error, null)
  }
}

/* Push Notification and Email Notification Common Service */
NotificationUtils.getLeadData = function (stepNumber, templateData, callback) {
  console.log('NotificationUtils.getLeadData FUNCTION -- CALLED')
  let thisFunctionName = 'NotificationUtils.getNotificationData'
  let Print = new ResponseUtils.Print(thisFileName, thisFunctionName)
  // let Response = new ResponseUtils.response(apiResponse)
  try {
    console.log('Reached to getLeadData function and receiving step number', stepNumber)
    if (stepNumber) {
      let matchFilter = []
      let today, yesterday
      today = new Date(Date.now())
      today = today.setHours(0, 0, 0, 0)
      console.log('Today-->', today)
      yesterday = new Date(today - 84600000)
      yesterday = yesterday.setHours(0, 0, 0, 0)
      console.log('YesterDay-->', yesterday)
      matchFilter.push({
        $match: {
          'last_completed_step': stepNumber
        }
      })
      matchFilter.push({
        $match: {
          'mobile': {
            $in: [
              '9493771266',
              '8465088024',
              '9494197969',
              '8788552599',
              '7416286692',
              '9966000280',
              '9492793427',
              '8686858945',
              '9951688800',
              '7893549123',
              '7675983284',
              '7207455147',
              '8788552599',
              '8328108263'
            ]
          }
        }
      })
      matchFilter.push({
        $group: {
          _id: '$mobile',
          full_name: {
            $first: '$full_name'
          },
          captured_date: {
            $first: '$captured_date'
          },
          leadID: {
            $first: '$_id'
          },
          mobile: {
            $first: '$mobile'
          },
          loan_type: {
            $first: '$loan_type'
          }
        }
      })
      // matchFilter.mobile = {
      //   $in: [
      //     '9493771266',
      //     '8465088024',
      //     '8686858945',
      //     '9494197969',
      //     '8788552599',
      //     '7416286692',
      //     '9966000280',
      //     '9492793427'
      //   ]
      // }

      // matchFilter.push({
      //   $match: {
      //     'step_updated_time': {
      //       $gte: yesterday,
      //       $lte: today
      //     }
      //   }
      // })
      console.log('MATCH FILTERSSSSSS  DATESS', matchFilter)
      ConsumerLeadsModel.aggregate(matchFilter, function (error, leadData) {
        // console.log('LEAD DATA AFTER EXECUTING-----------', leadData)
        if (error) {
          console.log('Error while fetching data for sending notification', error)
          return callback(error, null)
        } else if (leadData && leadData.length > 0) {
          let alertContent = templateData[0].alert_content
          let stepData = []
          async.eachSeries(leadData, function (leadData, asyncEachSeriesCallback) {
            if (stepNumber && stepNumber === 4) {
              console.log('Checking Offers for this lead -->', leadData.leadID)
              CommonService.checkOtherOffersforLead(false, leadData.loan_type, leadData.leadID, false, function (message, errorCode, error, data) {
                if (errorCode || error) {
                  console.log('Error occured while checking for offers')
                } else {
                  console.log('Lead Data at Step 4', leadData, leadData.length)
                  if (leadData && leadData.length > 0) {
                    NotificationUtils.notificationFormatPrepare(leadData, alertContent, function (error, data) {
                      if (error) {
                        console.log('Error while getting prepared formating data for this lead ID->', leadData.leadID, error)
                        asyncEachSeriesCallback()
                      } else {
                        if (data && data != null) {
                          console.log('Returning Formatted Data for lead id->', leadData.leadID)
                          stepData.push(data)
                          asyncEachSeriesCallback()
                        } else {
                          console.log('No Data Founded for this lead ID -> ', leadData.leadID)
                          asyncEachSeriesCallback()
                        }
                      }
                    })
                  } else {
                    console.log('NO OFFERS FOUNDED FOR THIS LEAD ID->', leadData.leadID)
                    asyncEachSeriesCallback()
                  }
                }
              })
            } else {
              console.log('Lead Data except 4th step', leadData, leadData.length)
              NotificationUtils.notificationFormatPrepare(leadData, alertContent, function (error, data) {
                if (error) {
                  console.log('Error while getting prepared formating data for this lead ID->', leadData.leadID, error)
                  asyncEachSeriesCallback()
                } else {
                  if (data && data != null) {
                    console.log('Returning Formatted Data', data)
                    stepData.push(data)
                    asyncEachSeriesCallback()
                  } else {
                    console.log('No Data Founded for this lead ID -> ', leadData.leadID)
                    asyncEachSeriesCallback()
                  }
                }
              })
            }
          }, function (error) {
            if (error) {
              console.log('Error while getting step data', error)
            } else {
              console.log('STEP DATA AND STEP DATA LENGTH', stepData, stepData.length)
              return callback(null, stepData)
            }
          })
        } else {
          return callback(null, null)
        }
      })
    } else {
      // return Response.success('NO BODY', null, 'NOBODY')
      console.log('Step Number is not Received')
      return callback(null, null)
    }
  } catch (e) {
    console.log('exception', e)
    Print.exception('Exception while gettings data for sending notifications', e)
  }
}
/* Waterfall function for getting stepwise data for sending notifications */
NotificationUtils.startService = function () {
  console.log('CRON RUNNING NOTIFICATION UITILSD')
  // NotificationUtils.stepWiseData = function (apiRequest, apiResponse) {
  console.log('INSIDE CRON FUNCTION')
  let thisFunctionName = 'NotificationUtils.stepWiseData'
  let Print = new ResponseUtils.Print(thisFileName, thisFunctionName)
  // let Response = new ResponseUtils.response(apiResponse)
  let Logs = new ResponseUtils.Logs(thisFileName, thisFunctionName)
  let defaultErrorCode = 'SETWISEDATAERROR'
  try {
    var stepData = {}
    let stepNumber, templateName
    templateName = 'application_resume'
    NotificationUtils.getTemplateData(templateName, function (error, templateData) {
      if (error) {
        console.log('error while getting template data', error)
      } else if (templateData) {
        console.log('Template Data Received ^^__^^', templateData)
        async.parallel([
          function (next) {
            console.log('Request for stepOne Data from async waterfall ^^__^^')
            stepNumber = 1
            NotificationUtils.getLeadData(stepNumber, templateData, function (error, nextStepData) {
              if (error) {
                console.log('Error while getting stepOne Data ^^__^^', error)
                next('Error', error)
              } else {
                if (nextStepData) {
                  console.log(' StepOne Data Received ^^__^^', nextStepData.length)
                  next(null, nextStepData)
                } else {
                  next(null, null)
                }
              }
            })
          },
          function (next) {
            // stepData.stepOne = nextStepData
            stepNumber = 2
            console.log('Request for stepTwo Data from async waterfall ^^__^^')
            NotificationUtils.getLeadData(stepNumber, templateData, function (error, nextStepData) {
              if (error) {
                console.log('Error while getting stepTwo Data ^^__^^', error)
                next('Error', error)
              } else {
                if (nextStepData) {
                  console.log(' StepTwo Data Received ^^__^^', nextStepData.length)
                  next(null, nextStepData)
                } else {
                  next(null, null)
                }
              }
            })
          },
          function (next) {
            // stepData.stepTwo = nextStepData
            stepNumber = 3
            console.log('Request for stepThree Data from async waterfall ^^__^^')
            NotificationUtils.getLeadData(stepNumber, templateData, function (error, nextStepData) {
              if (error) {
                console.log('Error while getting stepThree Data ^^__^^', error)
                next('Error', error)
              } else {
                if (nextStepData) {
                  console.log(' StepThree Data Received ^^__^^', nextStepData.length)
                  next(null, nextStepData)
                } else {
                  next(null, null)
                }
              }
            })
          },
          function (next) {
            // stepData.stepThree = nextStepData
            stepNumber = 4
            console.log('Request for stepFour Data from async waterfall ^^__^^')
            NotificationUtils.getLeadData(stepNumber, templateData, function (error, nextStepData) {
              if (error) {
                console.log('Error while getting stepFour Data ^^__^^', error)
                next('Error', error)
              } else {
                if (nextStepData) {
                  console.log(' StepFour Data Received ^^__^^', nextStepData.length)
                  next(null, nextStepData)
                } else {
                  next(null, null)
                }
              }
            })
          }
        ], function (err, stepData) {
          if (err) {
            Logs.error('ERROR WHILE WATERFALL RESULTS', defaultErrorCode, err + '', new Error())
          } else {
            if (stepData) {
              console.log('STEP DATAAAA-->|||||||||||||||||||||', stepData)
              NotificationUtils.pushDataToSending(stepData, function (error, data) {
                if (error) {
                  console.log('error')
                } else {
                  console.log('Data Completed', data)
                }
              })
            }
            console.log('Push notification sending is done for all steps data')
          }
        })
      } else {
        console.log('No Template Data')
      }
    })
  } catch (e) {
    Print.exception(e)
  }
  // }
}
/* Cron */
cron.schedule('0 30 5 * * MONDAY', function () {
  console.log('CRON Schedule Start ^^__^^')
  NotificationUtils.startService()
})
/* Template based notification content data */
NotificationUtils.getTemplateData = function (templateName, callback) {
  let thisFunctionName = 'NotificationUtils.getTemplateData'
  let Print = new ResponseUtils.Print(thisFileName, thisFunctionName)
  // let Response = new ResponseUtils.response(apiResponse)
  // let Logs = new ResponseUtils.Logs(thisFileName, thisFunctionName)
  // let defaultErrorCode = 'TEMPLTDATAERROR'
  console.log('Reached to getTemplateData function and template name is - >', templateName)
  try {
    if (templateName) {
      PushAlertModel.find({
        alert_name: templateName
      }, function (error, notificationData) {
        if (error) {
          return callback(error, null)
        } else if (notificationData) {
          return callback(null, notificationData)
        } else {
          return callback(null, null)
        }
      })
    } else {
      return callback(null, null)
    }
  } catch (e) {
    Print.exception(e)
    return callback(e, null)
  }
}
/* Pushing to send one by one */
NotificationUtils.pushDataToSending = function (totalData, callback) {
  let thisFunctionName = 'NotificationUtils.pushDataToSending'
  let Print = new ResponseUtils.Print(thisFileName, thisFunctionName)
  // let Response = new ResponseUtils.response(apiResponse)
  try {
    // console.log('Reached to PushDataTosending Function and total received data length is', totalData, totalData.length)
    let totalArray = []
    let totalLeadArray = []
    let count = 0
    Object.keys(totalData).forEach(function (key) {
      totalArray.push(totalData[key])
    })
    for (let i = 0; i < totalArray.length; i++) {
      totalLeadArray = totalLeadArray.concat(totalArray[i])
    }
    // console.log('TOTAL DATA AFTER ALL DONE!!', totalLeadArray)
    let pushNotificationURL = AppConfig.pushNotificationURL
    console.log('PUSH NOTIFICATION URL IS -> ', pushNotificationURL)
    if (totalLeadArray && totalLeadArray.length > 0) {
      async.eachSeries(totalLeadArray, function (stepData, asynSeriesCallback) {
        // console.log('STEP DATAA--<<<<<<', stepData)

        if (stepData) {
          console.log('Prepared Data before send', stepData)
          Request.post({
            url: pushNotificationURL,
            headers: {
              'Content-Type': 'application/json'
            },
            body: stepData,
            json: true
          }, function (error, smsResponse) {
            console.log('Request has been sent', error)
            if (error) {
              console.log('Errorrrrr while sending sms', error)
              asynSeriesCallback()
            } else if (smsResponse.body) {
              if (smsResponse.body.status === 200) {
                count++
                console.log('SENT SUCCESSFULLY', smsResponse.body.status)
                asynSeriesCallback()
              } else {
                asynSeriesCallback()
              }
            } else {
              asynSeriesCallback()
            }
          })
        } else {
          asynSeriesCallback()
        }
      }, function (err) {
        if (err) {
          console.log('error while sending sms to total data')
        } else {
          console.log('Done sending sms to total data and Successfully SMS SENT Count', count)
          return callback(null, 'Done')
        }
      })
    } else {
      return callback(null, 'Done')
    }
  } catch (e) {
    Print.exception('Exception while gettings data for sending notifications', e)
  }
}
/* PUSH NOTIFICATION FORMAT PREPARING */
NotificationUtils.notificationFormatPrepare = function (leadData, alertContent, callback) {
  console.log('Reached to notificationFormatPrepare function')
  if (leadData && leadData.captured_date && alertContent && leadData.mobile && leadData.mobile !== undefined) {
    let response = []
    let configuration = {}
    let capturedDate = new Date(leadData.captured_date)
    capturedDate = capturedDate.setHours(0, 0, 0, 0) // capture date timestamp
    let today = new Date(Date.now())
    today = today.setHours(0, 0, 0, 0) // today timestamp
    let timeDiff = today - capturedDate
    timeDiff = timeDiff / 60000 // convert milli seconds to minutes
    console.log('CaptureDate -', capturedDate, 'Today Time - ', today, 'Time Diff -', timeDiff)
    if (timeDiff <= 43200) {
      console.log('THIS LEAD IS BEFORE ONE MONTH->')
      configuration.replace = {}
      configuration.replace.name = leadData.full_name ? leadData.full_name : 'User'
      // configuration.replace.loanType = leadData.loan_type
      Object.keys(configuration.replace).forEach(function (obj) {
        alertContent = alertContent.replace('##' + obj + '##', configuration.replace[obj])
      })
      response.push({
        mobile: leadData.mobile,
        loan_type: leadData.loan_type,
        alert_content: alertContent,
        type: 'loans',
        version: '3.1'
      })
      // console.log('Formatted Lead Data ^^__^^ ', response[0])
      return callback(null, response[0])
    } else {
      callback(null, null)
    }
    // console.log('FULL NAME', leadData.full_name)
  } else {
    callback(null, null)
  }
}
