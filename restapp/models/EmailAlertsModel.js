var Mongoose = require('mongoose'),
  Schema = Mongoose.Schema,
  validator = require('mongoose-unique-validator'),
  autoIncrement = require('mongoose-auto-increment'),
  Float = require('mongoose-float').loadType(Mongoose);
var user_data = new Schema({
  user_name: {
    type: String,
    required: true
  },
}, { versionKey: false });

user_data.plugin(validator);
autoIncrement.initialize(Mongoose.connection);
user_data.index({ alert_name: 1 }, { name: 'alertname' });
user_data.plugin(autoIncrement.plugin, { model: 'user_data', field: 'ea_id', startAt: 1 });
module.exports = Mongoose.model('user_data', user_data);