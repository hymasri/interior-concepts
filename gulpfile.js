'use strict'
var gulp = require('gulp')
var watch = require('gulp-watch')
var nodemon = require('gulp-nodemon')
// var config = require('./config');
var runSequence = require('run-sequence')

gulp.task('nodemon', function (rs) {
  var started = false
  console.log('in gulp')
  return nodemon({
    script: 'server.js',
    ext: 'js json',
    env: { 'NODE_ENV': 'development' },
    ignore: [
      'src/',
      'dist/',
      'node_modules/'
    ],
    watch: ['./restapp/routes/routes.js', './restapp/models', './restapp/services', './restapp/utils', 'server.js'],
    stdout: true,
    readable: true
  }).on('start', function () {
    // to avoid nodemon being started multiple times
    if (!started) {
      rs()
      started = true
    }
  })
})

gulp.task('default', function () {
  console.log(process.env.NODE_ENV)
  let env = process.env.NODE_ENV
  if (!env) {
    env = 'development'
  }
  if (env === 'development') {
    runSequence('nodemon')
  } else {
  }
})
